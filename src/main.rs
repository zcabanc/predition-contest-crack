use std::{collections::HashSet, convert::TryInto, fs::read, sync::atomic::AtomicUsize, thread::sleep, time::Duration};
use std::{error::Error, fs::read_to_string};

use chrono::Datelike;
use chrono::NaiveDate;
use openssl::sha::sha256;

/// Convert a hex string with no whitespace or other sepreator into `&[u8]`.
///
/// # Example:
/// ```rust
/// assert_eq!(&hex_to_u8("aabb"), b"\xaa\xbb");
/// ```
pub fn hex_to_u8(hex: &str) -> Vec<u8> {
  if hex.len() % 2 != 0 {
    panic!("partial hex?");
  }
  let mut vec = Vec::new();
  vec.reserve(hex.len() / 2);
  for i in 0..(hex.len() / 2) {
    let hex = &hex[i * 2..(i + 1) * 2];
    vec.push(u8::from_str_radix(hex, 16).unwrap());
  }
  vec
}

/// Convert a `&[u8]` byte array to a lower-case, no-sepreator hex string.
///
/// # Example:
/// ```rust
/// assert_eq!(&u8_to_hex(b"\xaa\xbb"), "aabb");
/// ```
pub fn u8_to_hex(bytes: &[u8]) -> String {
  let mut buf = String::new();
  for i in bytes {
    buf.push_str(&format!("{:02x?}", i));
  }
  buf
}

pub fn prehash_password(password: &[u8], im_buf: &mut Vec<u8>) -> String {
  im_buf.clear();
  im_buf.extend_from_slice(password);
  im_buf.push(b'\n');
  let im = sha256(&im_buf);
  let hex = u8_to_hex(&im);
  hex
}

pub fn compute(guess_date: &str, prehash: &str, im_buf: &mut Vec<u8>) -> [u8; 32] {
  im_buf.clear();
  im_buf.extend_from_slice(guess_date.as_bytes());
  im_buf.extend_from_slice(prehash.as_bytes());
  im_buf.push(0x20);
  im_buf.push(0x20);
  im_buf.push(0x2d);
  im_buf.push(0x0a);
  sha256(&im_buf)
}

fn main() -> Result<(), Box<dyn Error>> {
  let hashes = read_to_string("hashes.txt")?;
  let hashes: HashSet<[u8; 32]> = hashes
    .lines()
    .map(|x| hex_to_u8(x).try_into().unwrap())
    .collect();
  let passwords = read("passwords.txt")?;
  let passwords = String::from_utf8_lossy(&passwords);
  let passwords = passwords.lines().collect::<Vec<_>>();
  println!("{} passwords.", passwords.len());
  let nb_threads = num_cpus::get();
  let mut dates = Vec::new();
  {
    let mut curr = NaiveDate::from_ymd(2021, 2, 1);
    while curr.year() == 2021 || curr.year() == 2022 && curr.month() == 1 {
      dates.push(curr.format("%Y-%m-%d").to_string());
      curr = curr.succ();
    }
  }
  println!("From {} to {}.", &dates[0], dates.last().unwrap());
  println!("Using {} threads.", nb_threads);
  let password_checked = AtomicUsize::new(0);
  crossbeam_utils::thread::scope(|scope| {
    let passwords = &passwords;
    let dates = &dates[..];
    let hashes = &hashes;
    let password_checked = &password_checked;
    for offset in 0..nb_threads {
      scope.spawn(move |_| {
        let mut im_buf = Vec::new();
        let mut i = offset;
        while i < passwords.len() {
          let password_to_try = passwords[i].as_bytes();
          let prehash = prehash_password(password_to_try, &mut im_buf);
          for date in dates {
            let h = compute(&date, &prehash, &mut im_buf);
            if hashes.contains(&h) {
              let s = u8_to_hex(&h);
              println!(
                "\n\nCracked {}: date = {}, passpharse = {}\n\n",
                s,
                date,
                std::str::from_utf8(password_to_try).unwrap()
              );
              break;
            }
          }
          i += nb_threads;
          password_checked.fetch_add(1, std::sync::atomic::Ordering::Relaxed);
        }
      });
    }
    loop {
      let current_passwords_checked = password_checked.load(std::sync::atomic::Ordering::Relaxed);
      if current_passwords_checked == passwords.len() {
        return;
      }
      eprint!("\x1b[2K\rChecked {} passwords.", current_passwords_checked);
      sleep(Duration::from_millis(50));
    }
  })
  .unwrap();
  Ok(())
}
